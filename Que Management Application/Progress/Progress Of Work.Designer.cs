﻿namespace Que_Management_Application.Progress
{
    partial class Progress_Of_Work
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.txtjobsearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtemp1name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtemp2name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtemp3name = new System.Windows.Forms.TextBox();
            this.lbler1 = new System.Windows.Forms.Label();
            this.btnback = new System.Windows.Forms.Button();
            this.btnsearch = new System.Windows.Forms.Button();
            this.lble3 = new System.Windows.Forms.Label();
            this.lble2 = new System.Windows.Forms.Label();
            this.lble1 = new System.Windows.Forms.Label();
            this.lbler2 = new System.Windows.Forms.Label();
            this.lbler3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtemp4name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtemp5name = new System.Windows.Forms.TextBox();
            this.lble4 = new System.Windows.Forms.Label();
            this.lble5 = new System.Windows.Forms.Label();
            this.lbler4 = new System.Windows.Forms.Label();
            this.lbler5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Snow;
            this.label6.Location = new System.Drawing.Point(124, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 26);
            this.label6.TabIndex = 16;
            this.label6.Text = "Search By Jobcode";
            // 
            // txtjobsearch
            // 
            this.txtjobsearch.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjobsearch.Location = new System.Drawing.Point(257, 24);
            this.txtjobsearch.Name = "txtjobsearch";
            this.txtjobsearch.Size = new System.Drawing.Size(183, 33);
            this.txtjobsearch.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(125, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 26);
            this.label1.TabIndex = 18;
            this.label1.Text = "Employee 1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtemp1name
            // 
            this.txtemp1name.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp1name.Location = new System.Drawing.Point(257, 103);
            this.txtemp1name.Name = "txtemp1name";
            this.txtemp1name.Size = new System.Drawing.Size(183, 33);
            this.txtemp1name.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(125, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 26);
            this.label2.TabIndex = 20;
            this.label2.Text = "Employee 2";
            // 
            // txtemp2name
            // 
            this.txtemp2name.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp2name.Location = new System.Drawing.Point(257, 149);
            this.txtemp2name.Name = "txtemp2name";
            this.txtemp2name.Size = new System.Drawing.Size(183, 33);
            this.txtemp2name.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(125, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 26);
            this.label3.TabIndex = 22;
            this.label3.Text = "Employee 3";
            // 
            // txtemp3name
            // 
            this.txtemp3name.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp3name.Location = new System.Drawing.Point(257, 196);
            this.txtemp3name.Name = "txtemp3name";
            this.txtemp3name.Size = new System.Drawing.Size(183, 33);
            this.txtemp3name.TabIndex = 21;
            // 
            // lbler1
            // 
            this.lbler1.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00d_0__32;
            this.lbler1.Location = new System.Drawing.Point(446, 103);
            this.lbler1.Name = "lbler1";
            this.lbler1.Size = new System.Drawing.Size(38, 33);
            this.lbler1.TabIndex = 29;
            // 
            // btnback
            // 
            this.btnback.BackgroundImage = global::Que_Management_Application.Properties.Resources.Back;
            this.btnback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnback.Location = new System.Drawing.Point(12, 136);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(49, 42);
            this.btnback.TabIndex = 28;
            this.btnback.UseVisualStyleBackColor = true;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnsearch
            // 
            this.btnsearch.BackgroundImage = global::Que_Management_Application.Properties.Resources.Entypo_d83d_0__512;
            this.btnsearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnsearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsearch.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsearch.Location = new System.Drawing.Point(446, 24);
            this.btnsearch.Name = "btnsearch";
            this.btnsearch.Size = new System.Drawing.Size(42, 35);
            this.btnsearch.TabIndex = 26;
            this.btnsearch.UseVisualStyleBackColor = true;
            this.btnsearch.Click += new System.EventHandler(this.btnsearch_Click);
            // 
            // lble3
            // 
            this.lble3.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00c_0__32;
            this.lble3.Location = new System.Drawing.Point(446, 196);
            this.lble3.Name = "lble3";
            this.lble3.Size = new System.Drawing.Size(38, 33);
            this.lble3.TabIndex = 25;
            // 
            // lble2
            // 
            this.lble2.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00c_0__32;
            this.lble2.Location = new System.Drawing.Point(446, 149);
            this.lble2.Name = "lble2";
            this.lble2.Size = new System.Drawing.Size(38, 33);
            this.lble2.TabIndex = 24;
            // 
            // lble1
            // 
            this.lble1.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00c_0__32;
            this.lble1.Location = new System.Drawing.Point(446, 103);
            this.lble1.Name = "lble1";
            this.lble1.Size = new System.Drawing.Size(38, 33);
            this.lble1.TabIndex = 23;
            // 
            // lbler2
            // 
            this.lbler2.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00d_0__32;
            this.lbler2.Location = new System.Drawing.Point(446, 151);
            this.lbler2.Name = "lbler2";
            this.lbler2.Size = new System.Drawing.Size(38, 33);
            this.lbler2.TabIndex = 30;
            // 
            // lbler3
            // 
            this.lbler3.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00d_0__32;
            this.lbler3.Location = new System.Drawing.Point(446, 196);
            this.lbler3.Name = "lbler3";
            this.lbler3.Size = new System.Drawing.Size(38, 33);
            this.lbler3.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Snow;
            this.label4.Location = new System.Drawing.Point(125, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 26);
            this.label4.TabIndex = 33;
            this.label4.Text = "Employee 4";
            // 
            // txtemp4name
            // 
            this.txtemp4name.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp4name.Location = new System.Drawing.Point(257, 245);
            this.txtemp4name.Name = "txtemp4name";
            this.txtemp4name.Size = new System.Drawing.Size(183, 33);
            this.txtemp4name.TabIndex = 32;
            this.txtemp4name.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Snow;
            this.label5.Location = new System.Drawing.Point(125, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 26);
            this.label5.TabIndex = 35;
            this.label5.Text = "Employee 5";
            // 
            // txtemp5name
            // 
            this.txtemp5name.Font = new System.Drawing.Font("Adobe Caslon Pro", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemp5name.Location = new System.Drawing.Point(257, 294);
            this.txtemp5name.Name = "txtemp5name";
            this.txtemp5name.Size = new System.Drawing.Size(183, 33);
            this.txtemp5name.TabIndex = 34;
            // 
            // lble4
            // 
            this.lble4.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00c_0__32;
            this.lble4.Location = new System.Drawing.Point(446, 245);
            this.lble4.Name = "lble4";
            this.lble4.Size = new System.Drawing.Size(38, 33);
            this.lble4.TabIndex = 36;
            // 
            // lble5
            // 
            this.lble5.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00c_0__32;
            this.lble5.Location = new System.Drawing.Point(446, 294);
            this.lble5.Name = "lble5";
            this.lble5.Size = new System.Drawing.Size(38, 33);
            this.lble5.TabIndex = 37;
            // 
            // lbler4
            // 
            this.lbler4.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00d_0__32;
            this.lbler4.Location = new System.Drawing.Point(446, 245);
            this.lbler4.Name = "lbler4";
            this.lbler4.Size = new System.Drawing.Size(38, 33);
            this.lbler4.TabIndex = 38;
            // 
            // lbler5
            // 
            this.lbler5.Image = global::Que_Management_Application.Properties.Resources.FontAwesome_f00d_0__32;
            this.lbler5.Location = new System.Drawing.Point(446, 294);
            this.lbler5.Name = "lbler5";
            this.lbler5.Size = new System.Drawing.Size(38, 33);
            this.lbler5.TabIndex = 39;
            // 
            // Progress_Of_Work
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(732, 376);
            this.Controls.Add(this.lbler5);
            this.Controls.Add(this.lbler4);
            this.Controls.Add(this.lble5);
            this.Controls.Add(this.lble4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtemp5name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtemp4name);
            this.Controls.Add(this.lbler3);
            this.Controls.Add(this.lbler2);
            this.Controls.Add(this.lbler1);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.btnsearch);
            this.Controls.Add(this.lble3);
            this.Controls.Add(this.lble2);
            this.Controls.Add(this.lble1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtemp3name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtemp2name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtemp1name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtjobsearch);
            this.Name = "Progress_Of_Work";
            this.Text = "Progress_Of_Work";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Progress_Of_Work_FormClosed);
            this.Load += new System.EventHandler(this.Progress_Of_Work_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtjobsearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtemp1name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtemp2name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtemp3name;
        private System.Windows.Forms.Label lble1;
        private System.Windows.Forms.Label lble2;
        private System.Windows.Forms.Label lble3;
        private System.Windows.Forms.Button btnsearch;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Label lbler1;
        private System.Windows.Forms.Label lbler2;
        private System.Windows.Forms.Label lbler3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtemp4name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtemp5name;
        private System.Windows.Forms.Label lble4;
        private System.Windows.Forms.Label lble5;
        private System.Windows.Forms.Label lbler4;
        private System.Windows.Forms.Label lbler5;
    }
}