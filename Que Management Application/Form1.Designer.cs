﻿namespace Que_Management_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtcustomername = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtallocatetime = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtstarttime = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.btndual = new System.Windows.Forms.Button();
            this.btnonejob = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtemp3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtemp2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtemp1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtjobcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.Time1 = new System.Windows.Forms.Timer(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.btnback = new System.Windows.Forms.Button();
            this.btnshow = new System.Windows.Forms.Button();
            this.btnstart = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtemp4 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtemp5 = new System.Windows.Forms.TextBox();
            this.pnlshow = new System.Windows.Forms.Panel();
            this.tbldetails = new System.Windows.Forms.DataGridView();
            this.btnok = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlshow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbldetails)).BeginInit();
            this.SuspendLayout();
            // 
            // txtcustomername
            // 
            this.txtcustomername.Location = new System.Drawing.Point(134, 28);
            this.txtcustomername.Name = "txtcustomername";
            this.txtcustomername.Size = new System.Drawing.Size(243, 20);
            this.txtcustomername.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtitem);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtallocatetime);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtstarttime);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtjobcode);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtcustomername);
            this.groupBox1.ForeColor = System.Drawing.Color.Snow;
            this.groupBox1.Location = new System.Drawing.Point(68, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(594, 254);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customer Information";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(42, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Item";
            // 
            // txtitem
            // 
            this.txtitem.Location = new System.Drawing.Point(134, 95);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(243, 20);
            this.txtitem.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(218, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Allocate Time";
            // 
            // txtallocatetime
            // 
            this.txtallocatetime.Location = new System.Drawing.Point(310, 63);
            this.txtallocatetime.Name = "txtallocatetime";
            this.txtallocatetime.Size = new System.Drawing.Size(67, 20);
            this.txtallocatetime.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Starting time";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtstarttime
            // 
            this.txtstarttime.Location = new System.Drawing.Point(134, 63);
            this.txtstarttime.Name = "txtstarttime";
            this.txtstarttime.Size = new System.Drawing.Size(67, 20);
            this.txtstarttime.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.btndual);
            this.groupBox2.Controls.Add(this.btnonejob);
            this.groupBox2.ForeColor = System.Drawing.Color.Honeydew;
            this.groupBox2.Location = new System.Drawing.Point(428, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(135, 105);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Job Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Full";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Duel";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Singale ";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(51, 69);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 19;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btndual
            // 
            this.btndual.BackColor = System.Drawing.Color.DarkOrange;
            this.btndual.Location = new System.Drawing.Point(51, 37);
            this.btndual.Name = "btndual";
            this.btndual.Size = new System.Drawing.Size(75, 23);
            this.btndual.TabIndex = 18;
            this.btndual.UseVisualStyleBackColor = false;
            this.btndual.Click += new System.EventHandler(this.btndual_Click);
            // 
            // btnonejob
            // 
            this.btnonejob.BackColor = System.Drawing.Color.LimeGreen;
            this.btnonejob.Location = new System.Drawing.Point(51, 8);
            this.btnonejob.Name = "btnonejob";
            this.btnonejob.Size = new System.Drawing.Size(75, 23);
            this.btnonejob.TabIndex = 17;
            this.btnonejob.UseVisualStyleBackColor = false;
            this.btnonejob.Click += new System.EventHandler(this.btnonejob_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtemp5);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtemp4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtemp3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtemp2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtemp1);
            this.panel1.Location = new System.Drawing.Point(45, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(372, 123);
            this.panel1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Employee 3";
            // 
            // txtemp3
            // 
            this.txtemp3.Location = new System.Drawing.Point(92, 66);
            this.txtemp3.Name = "txtemp3";
            this.txtemp3.Size = new System.Drawing.Size(89, 20);
            this.txtemp3.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Employee 2";
            // 
            // txtemp2
            // 
            this.txtemp2.Location = new System.Drawing.Point(92, 40);
            this.txtemp2.Name = "txtemp2";
            this.txtemp2.Size = new System.Drawing.Size(89, 20);
            this.txtemp2.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Employee 1";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtemp1
            // 
            this.txtemp1.Location = new System.Drawing.Point(92, 13);
            this.txtemp1.Name = "txtemp1";
            this.txtemp1.Size = new System.Drawing.Size(89, 20);
            this.txtemp1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Job Number";
            // 
            // txtjobcode
            // 
            this.txtjobcode.Location = new System.Drawing.Point(496, 28);
            this.txtjobcode.Name = "txtjobcode";
            this.txtjobcode.Size = new System.Drawing.Size(67, 20);
            this.txtjobcode.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Customer Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Minion Pro Med", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(238, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(247, 28);
            this.label11.TabIndex = 6;
            this.label11.Text = "Que Management  System";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltime.Location = new System.Drawing.Point(12, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(51, 15);
            this.lbltime.TabIndex = 8;
            this.lbltime.Text = "lbltime";
            // 
            // Time1
            // 
            this.Time1.Tick += new System.EventHandler(this.Time1_Tick);
            // 
            // btnback
            // 
            this.btnback.BackgroundImage = global::Que_Management_Application.Properties.Resources.Back;
            this.btnback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnback.Location = new System.Drawing.Point(3, 167);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(49, 42);
            this.btnback.TabIndex = 13;
            this.btnback.UseVisualStyleBackColor = true;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnshow
            // 
            this.btnshow.BackgroundImage = global::Que_Management_Application.Properties.Resources.cale;
            this.btnshow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnshow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnshow.Location = new System.Drawing.Point(469, 369);
            this.btnshow.Name = "btnshow";
            this.btnshow.Size = new System.Drawing.Size(81, 78);
            this.btnshow.TabIndex = 3;
            this.btnshow.UseVisualStyleBackColor = true;
            this.btnshow.Click += new System.EventHandler(this.btnshow_Click);
            // 
            // btnstart
            // 
            this.btnstart.BackgroundImage = global::Que_Management_Application.Properties.Resources.FontAwesome_f04b_0__512;
            this.btnstart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnstart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnstart.Location = new System.Drawing.Point(309, 334);
            this.btnstart.Name = "btnstart";
            this.btnstart.Size = new System.Drawing.Size(120, 113);
            this.btnstart.TabIndex = 2;
            this.btnstart.UseVisualStyleBackColor = true;
            this.btnstart.Click += new System.EventHandler(this.btnstart_Click);
            // 
            // btncancel
            // 
            this.btncancel.BackgroundImage = global::Que_Management_Application.Properties.Resources.Entypo_2716_0__512;
            this.btncancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btncancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncancel.Location = new System.Drawing.Point(186, 369);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(82, 78);
            this.btncancel.TabIndex = 0;
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Employee 4";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // txtemp4
            // 
            this.txtemp4.Location = new System.Drawing.Point(92, 92);
            this.txtemp4.Name = "txtemp4";
            this.txtemp4.Size = new System.Drawing.Size(89, 20);
            this.txtemp4.TabIndex = 12;
            this.txtemp4.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(192, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Employee 5";
            // 
            // txtemp5
            // 
            this.txtemp5.Location = new System.Drawing.Point(280, 13);
            this.txtemp5.Name = "txtemp5";
            this.txtemp5.Size = new System.Drawing.Size(89, 20);
            this.txtemp5.TabIndex = 14;
            // 
            // pnlshow
            // 
            this.pnlshow.Controls.Add(this.btnok);
            this.pnlshow.Controls.Add(this.tbldetails);
            this.pnlshow.Location = new System.Drawing.Point(69, 32);
            this.pnlshow.Name = "pnlshow";
            this.pnlshow.Size = new System.Drawing.Size(605, 255);
            this.pnlshow.TabIndex = 16;
            // 
            // tbldetails
            // 
            this.tbldetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tbldetails.Location = new System.Drawing.Point(4, 0);
            this.tbldetails.Name = "tbldetails";
            this.tbldetails.Size = new System.Drawing.Size(598, 213);
            this.tbldetails.TabIndex = 0;
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(270, 219);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(735, 474);
            this.Controls.Add(this.pnlshow);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnshow);
            this.Controls.Add(this.btnstart);
            this.Controls.Add(this.btncancel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlshow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbldetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnstart;
        private System.Windows.Forms.Button btnshow;
        private System.Windows.Forms.TextBox txtcustomername;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btndual;
        private System.Windows.Forms.Button btnonejob;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtemp3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtemp2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtemp1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtjobcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtallocatetime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtstarttime;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer Time1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtitem;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtemp4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtemp5;
        private System.Windows.Forms.Panel pnlshow;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.DataGridView tbldetails;
    }
}

