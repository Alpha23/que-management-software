﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Que_Management_Application.Dashboard
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
            timer1.Start();
            Fillcombo();
        }
       
        void Fillcombo()
        {
            string Status = "0";
            string constring = "datasource=localhost;port=3306;user=root;password=";
            string query = "SELECT * FROM Que_Management.Employee where Status='" + Status + "'";
            MySqlConnection conn = new MySqlConnection(constring);
            MySqlCommand com = new MySqlCommand(query, conn);
            MySqlDataReader myReader;
            try
            {
                conn.Open();
                myReader = com.ExecuteReader();
                while (myReader.Read())
                {
                    string vemployee = myReader.GetString("EmpName");
                    lbemployee.Items.Add(vemployee);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime datetime = DateTime.Now;
            this.lblTime.Text = datetime.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Progress.Progress_Of_Work pw = new Progress.Progress_Of_Work();
            pw.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FinishItem.Finish_Item fi = new FinishItem.Finish_Item();
            fi.Show();

        }
    }
}
